package Tema1;

public enum tipContainer {

    Mic_10mc("Mic_10mc"),
    Mediu_25mc("Mediu_25mc"),
    Mare_50mc("Mare_50mc"),
    Jumbo_100mc("Jumbo_100mc");

    private String valoare;

    tipContainer(String valoare) {

        this.valoare = valoare;
    }

    @Override
    public String toString() {
        return valoare.toString();
    }

    public int getCapacitate() {
        switch (this) {
            case Mic_10mc:
                return 10;
            case Mediu_25mc:
                return 25;
            case Mare_50mc:
                return 50;
            case Jumbo_100mc:
                return 100;
        }
        return 0;
    }


}