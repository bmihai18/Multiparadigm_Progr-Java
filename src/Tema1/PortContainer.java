package Tema1;

import java.util.*;
import java.io.*;
import java.lang.*;

public class PortContainer implements Cloneable, Numarabil {
    private String eticheta;
    private tipContainer[] tipContainer;
    private int[] nrContainere;

    public PortContainer(String eticheta) {
        this.eticheta = eticheta;
    }

    @Override
    public String toString() {
 String local="";
 for (int i=0; i<tipContainer.length; i++) {

         local += "," + tipContainer[i].toString() + "," + Integer.toString(nrContainere[i]);
        }
        return eticheta  + local;
    }


    public PortContainer() {
        this.eticheta = null;
        this.tipContainer = null;
        this.nrContainere = null;
    }

    public String getEticheta() {
        return eticheta;
    }

    public void setEticheta(String eticheta) {
        this.eticheta = eticheta;
    }

    public tipContainer[] getTipContainer() {
        return tipContainer;
    }

    public void setTipContainer(tipContainer[] tipC) {
        this.tipContainer = tipC;
    }

    public int[] getNrContainere() {
        return nrContainere;
    }

    public void setNrContainere(int[] nrContainere) {
        this.nrContainere =  nrContainere;
    }

    @Override
    public int getCapacitate() {

        int rezultat = 0;
        for (int i = 0; i < 4; i++) {
            rezultat = nrContainere[i] * tipContainer[i].getCapacitate();
        }

        return rezultat;

    }


}










