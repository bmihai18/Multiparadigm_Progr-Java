package Tema1;
public class Macara {
    private tipContainer[] tipContainer;
    private int timpManipulare;


    public Macara(String valoare) {
        this.timpManipulare = 0;
        this.tipContainer=null;


    }

    public tipContainer[] getTipContainer() {
        return tipContainer;
    }

    public void setTipContainer(tipContainer[] tipContainer) {
        this.tipContainer = tipContainer;
    }

    public int getTimpManipulare() {
        return timpManipulare;
    }

    public void setTimpManipulare(int timpManipulare) {
        this.timpManipulare = timpManipulare;
    }

    public Macara(tipContainer[] tipContainer, int timpManipulare) {
        this.tipContainer = tipContainer;
        this.timpManipulare = timpManipulare;
    }
}
