package Tema.EchipaFotbal;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import proiect.Echipa;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.List;

public class ControllerClient {
    @FXML private TextField tNume ;
    @FXML private TextArea tout ;
    @FXML private TextArea toateEchipeleLista ;
    @FXML private void cerere(){
        try(Socket socket = new Socket("localhost",2013)){
            try(ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
                ObjectInputStream in = new ObjectInputStream(socket.getInputStream())){
                out.writeObject(tNume.getText().trim());
                List<Echipa> lista = (List<Echipa>) in.readObject();
                if(tNume.getText().length()>0)
                {
                    tout.clear();
                    lista.forEach(echipa -> tout.appendText(echipa.toString() + "\n"));
                }
            }
        }
        catch (Exception ex){
            Alert err = new Alert(Alert.AlertType.ERROR,ex.toString());
            err.showAndWait();
        }

    }
    @FXML private void toateEchipele(){
        try(Socket socket = new Socket("localhost",2013)){
            try(ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
                ObjectInputStream in = new ObjectInputStream(socket.getInputStream())){
                out.writeObject("");
                List<Echipa> lista = (List<Echipa>) in.readObject();
                toateEchipeleLista.clear();
                lista.forEach(echipa -> toateEchipeleLista.appendText(echipa.toString()+"\n"));
            }
        }
        catch (Exception ex){
            Alert err = new Alert(Alert.AlertType.ERROR,ex.toString());
            err.showAndWait();
        }

    }
}
