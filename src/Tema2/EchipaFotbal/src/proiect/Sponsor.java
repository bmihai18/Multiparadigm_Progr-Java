package proiect;

public class Sponsor {
    String numeSponsor ;
    int suma ;

    public Sponsor(String numeSponsor, int suma) {
        this.numeSponsor = numeSponsor;
        this.suma = suma;
    }

    public Sponsor() {
    }

    public String getNumeSponsor() {
        return numeSponsor;
    }

    public void setNumeSponsor(String numeSponsor) {
        this.numeSponsor = numeSponsor;
    }

    public int getSuma() {
        return suma;
    }

    public void setSuma(int suma) {
        this.suma = suma;
    }

    @Override
    public String toString() {
        return "Sponsor{" +
                "numeSponsor='" + numeSponsor + '\'' +
                ", suma=" + suma +
                '}';
    }
}
