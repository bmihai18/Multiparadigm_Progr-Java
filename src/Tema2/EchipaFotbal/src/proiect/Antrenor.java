package proiect;

public class Antrenor implements Salarizabil{
    private String numeAntrenor ;
    private int varsta ;
    public String getNumeAntrenor() {
        return numeAntrenor;
    }
    public void setNumeAntrenor(String numeAntrenor) {
        this.numeAntrenor = numeAntrenor;
    }
    public int getVarsta() {
        return varsta;
    }
    public void setVarsta(int varsta) {
        this.varsta = varsta;
    }
    public Antrenor(String numeAntrenor, int varsta) {
        this.numeAntrenor = numeAntrenor;
        this.varsta = varsta;
    }
    @Override
    public int hashCode() {
        return super.hashCode();
    }
    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
    @Override
    protected Object clone() throws CloneNotSupportedException {

        Antrenor copy = new Antrenor();
        copy.varsta= this.varsta ;
        copy.numeAntrenor = this.numeAntrenor ;
        return super.clone();
    }
    @Override
    public String toString() {
        return super.toString();
    }
    public Antrenor() {
    }
    @Override
    public int calculeazaSalariul() {
        return  varsta* 100 ;
    }

}
