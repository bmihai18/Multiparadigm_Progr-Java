package proiect;

import java.util.List;

public class Campionat {
    private List<Echipa> listaEchipe ;
    String tara ;

    public Campionat(List<Echipa> listaEchipe, String tara) {
        this.listaEchipe = listaEchipe;
        this.tara = tara;
    }

    public Campionat() {
    }

    public List<Echipa> getListaEchipe() {
        return listaEchipe;
    }

    public void setListaEchipe(List<Echipa> listaEchipe) {
        this.listaEchipe = listaEchipe;
    }

    public String getTara() {
        return tara;
    }

    public void setTara(String tara) {
        this.tara = tara;
    }

    @Override
    public String toString() {
        return "Campionat{" +
                "listaEchipe=" + listaEchipe +
                ", tara='" + tara + '\'' +
                '}';
    }
}
