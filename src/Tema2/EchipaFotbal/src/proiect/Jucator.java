package proiect;

public class Jucator extends Persoana  implements Salarizabil{
    private int nrTricou ;

    public int getVarsta() {
        return varsta;
    }

    public void setVarsta(int varsta) {
        this.varsta = varsta;
    }

    public int getNrTricou() {
        return nrTricou;
    }

    public void setNrTricou(int nrTricou) {
        this.nrTricou = nrTricou;
    }

    public Jucator() {
    }

    public Jucator(String nume, int varsta) {
        super(nume, varsta);
        nrTricou=0;
    }

    public Jucator(String nume, int varsta, int nrTricou) {
        super(nume, varsta);
        this.nrTricou = nrTricou;
    }

    @Override
    public int calculeazaSalariul()
    {
        return varsta*200 ;
    }

    @Override
    public String toString() {
        return "Jucator{" +
                "nrTricou=" + nrTricou +
                ", nume='" + nume + '\'' +
                ", varsta=" + varsta +
                '}';
    }
}

