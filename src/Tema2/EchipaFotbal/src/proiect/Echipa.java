package proiect;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class Echipa implements Serializable ,Comparable<Echipa>{
    private int id ;
    private String nume ;
    private String antrenor ;
    private String sponsor ;
    private List<Jucator> jucatori ;

    public Echipa(int id, String nume, String antrenor, String sponsor, List<Jucator> jucatori) {
        this.id = id;
        this.nume = nume;
        this.antrenor = antrenor;
        this.sponsor = sponsor;
        this.jucatori = jucatori;
    }



    public void setJucatori(List<Jucator> jucatori) {
        this.jucatori = jucatori;
    }

    public String getSponsor() {
        return sponsor;
    }

    public void setSponsor(String sponsor) {
        this.sponsor = sponsor;
    }

    public Echipa() {
    }

    public String getAntrenor() {
        return antrenor;
    }

    public void setAntrenor(String antrenor) {
        this.antrenor = antrenor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    @Override
    public String toString() {
        return "Echipa "+ nume+" are id-ul "+id+", este antrenata de "+antrenor+" si sponsorizata de "+sponsor ;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Echipa)) return false;
        Echipa echipa = (Echipa) o;
        return id == echipa.id &&
                Objects.equals(nume, echipa.nume) &&
                Objects.equals(antrenor, echipa.antrenor) &&
                Objects.equals(sponsor, echipa.sponsor);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nume, antrenor, sponsor);
    }

    @Override
    public int compareTo(Echipa echipa) {
        if(id< echipa.id) {             return -1;         }
        else if(id == echipa.id) {             return 0;         }
        else {             return 1;         }
    }
}
