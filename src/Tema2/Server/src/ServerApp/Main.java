package ServerApp;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;



import java.net.URL;
import java.sql.*;

public class Main extends Application {
    private Connection c ;
    private ControllerServer_ controlerServer ;

    @Override
    public void init() throws Exception {
        c = DriverManager.getConnection("jdbc:sqlite:DBEchipaFotbal.db");
        DatabaseMetaData metaData = c.getMetaData();
        try (ResultSet r = metaData.getTables(null, null, "Echipa", new String[]{"TABLE"}))
        {
//            Verificare existenta tabela cu numele echipa
            if (!r.next()) {
                try (Statement s = c.createStatement()) {
                    String comandaCreare = "create table Echipa(id number(6),nume varchar(50),antrenor varchar(30),sponsor varchar(30)) ; ";
                    s.executeUpdate(comandaCreare);
                }
            }
            else {
                try (Statement s = c.createStatement()) {
                    s.executeUpdate("delete from echipa ; ");
                }
            }
            try (Statement s = c.createStatement()) {

                String comandaInserare = "insert into Echipa values(1,'FC Barcelona', 'Dragos' ,'George')";
                s.executeUpdate(comandaInserare);
                String comandaInserare1 = "insert into Echipa values(2 , 'Real Madrid', 'Dragos' , 'Andrei') " ;
                s.executeUpdate(comandaInserare1);
                String comandaInserare2 = "insert into Echipa values(3 , 'FC Steaua', 'Bergodi' , 'Claudiu') " ;
                s.executeUpdate(comandaInserare2);
                String comandaInserare3 = "insert into Echipa values(4 , 'FC Dinamo', 'Lucescu' , 'Mihai') " ;
                s.executeUpdate(comandaInserare3);
                String comandaInserare4 = "insert into Echipa values(5 , 'FC Rapid', 'Razvan Raz' , 'Copos') " ;
                s.executeUpdate(comandaInserare4);
                String comandaInserare5 = "insert into Echipa values(6 , 'FC ASE', 'Felix' , 'Daniel') " ;
                s.executeUpdate(comandaInserare5);
            }
        }
    }




    @Override
    public void stop() throws Exception {
        if(c != null){
            c.close();
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception{
        URL locatie = getClass().getResource("sample.fxml") ;
        FXMLLoader loader = new FXMLLoader(locatie) ;
        Parent root = loader.load() ;
        controlerServer = (ControllerServer_) loader.getController() ;
        controlerServer.setC(c);
        primaryStage.setTitle("SERVER");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();

    }


    public static void main(String[] args) {
        launch(args);
    }
}
