package ServerApp;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextArea;
import proiect.Echipa;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ControllerServer_ {
    private Connection c ;
    private List<Echipa> lista= new ArrayList<>() ;
    @FXML private TextArea tout ;
    private ServerSocket serverSocket;
    private boolean serverActiv = false;
    void procesareCerere(Socket socket) {
        try (ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
             ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream())) {
            String nume = in.readObject().toString();
            List<Echipa> listaRaspuns = lista.stream().filter(echipa -> echipa.getNume().contains(nume))
                    .collect(Collectors.toList());
            out.writeObject(listaRaspuns);
        } catch (Exception ex) {
            Platform.runLater(()->{
                tout.appendText(ex.toString()+"\n");
            });
        }
        finally {
            try{
                socket.close();
            }
            catch (Exception ex){}
        }
    }

    private void ascultare() {
        while (serverActiv) {
            try {
                Socket socket = serverSocket.accept();
                Thread firCerere = new Thread(() -> procesareCerere(socket));
                firCerere.start();
            } catch (Exception ex) {
            }
        }
    }

    @FXML private void stop() {
        if (!serverActiv) {
            return;
        }
        try {
            serverSocket.close();
        } catch (Exception ex) {
        }
        serverActiv = false;
        tout.appendText("Stop Server!\n");

    }
    @FXML private void start(){
        if (serverActiv) {
            tout.appendText("Server in functiune!\n");
            return;
        }
        try {
            serverSocket = new ServerSocket(2013);
            serverSocket.setSoTimeout(10000);
            tout.appendText("Server startat!\n");
            serverActiv = true;
            Thread firAscultare = new Thread(() -> ascultare());
            firAscultare.setDaemon(true);
            firAscultare.start();
        } catch (Exception ex) {
            Alert err = new Alert(Alert.AlertType.ERROR, ex.toString());
            err.showAndWait();
        }

    }
    public void setC(Connection c) {
        this.c = c;
        try (Statement s = c.createStatement();
             ResultSet r = s.executeQuery("select id,nume,antrenor,sponsor from echipa")) {
            while (r.next()) {
                Echipa echipa = new Echipa();
                echipa.setId(r.getInt(1));
                echipa.setNume(r.getString(2));
                echipa.setAntrenor(r.getString(3));
                echipa.setSponsor(r.getString(4));
                lista.add(echipa);
            }
        } catch (Exception ex) {
            Alert err = new Alert(Alert.AlertType.ERROR, ex.toString());
            err.showAndWait();
        }


    }
}
